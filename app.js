//require('systemd');
//require('autoquit');
//var journal = require('journald').log;
var express = require('express');
var twig = require('twig');
var path = require('path');
var bodyParser = require('body-parser');
var util = require('util');
var compression = require('compression');
var cluster = require('cluster');
var control = require('strong-cluster-control');
var app = express();
var timeout = require('connect-timeout'); //express v4
var session = require('express-session');
var ClusterStore = require('strong-cluster-connect-store')(session);

// global setup here...
app.use(timeout(120000));
app.disable('x-powered-by');
app.set('views',__dirname + '/app/views');
app.set('view engine', 'html');
app.engine('html', twig.__express);

app.set('staticdir',path.join(__dirname, 'public'));


app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());
app.use(compression({ level: 9 }));
var store = new ClusterStore();
app.set('store',store);

app.use(session({
 //path: '/',
 store: store,
 secret: 'ilyHLKJgfjkdek467JKRDh',
 //cookie: { maxAge: 900000, secure: true },
 resave: true,
  duration: 60 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
  //httpOnly: true,
  //secure: true,
  //ephemeral: true,
 saveUninitialized: false
}));


require('console-stamp')(console, 'dd/mm/yyyy HH:MM:ss.l');
require('./config/settings.js')(app,express);
require('./config/environment.js')(app, express);

app.use('/static',express.static(path.join(__dirname, 'public'),{ dotfiles: 'allow' }));
app.use('/Videos/perso/files',express.static(path.join(__dirname, 'private/files'),{ dotfiles: 'allow' }));
app.set('privatedir',path.join(__dirname, 'private'));

control.start({
  size: control.CPUS,
  shutdownTimeout: 5000,
  terminateTimeout: 5000,
  throttleDelay: 5000
}).on('error', function(er) {
  // don’t need to manually restart the workers
});

if (cluster.isWorker) {
  app.listen(app.get('port'), function(){
  	console.log(require('process').pid + "/ Express server listening on port " + app.get('port'))
	//console.log("Express server listening on port " + app.get('port'));
  });
}
