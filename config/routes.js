var path = require('path');
var url = require('url');
//var http = require('follow-redirects').http;
//var modRewrite = require('connect-modrewrite');

var UserAuth = callDataModel('userauth.js');
var VideoPerso = callController('Videos.perso.js');
var videoPerso = new VideoPerso();

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}


module.exports = function(app, express, passport) {
    var path =__dirname + '/../app/controllers';

    app.get('/Videos', function(req, res) {
        callController('Videos.js').accueil(app, req, res);
	log(app, req, res);
    });

    app.get('/Videos/logout', function(req, res) {
	callController('userauth.js').logout(app, req, res);
	log(app, req, res);
    });

    app.post('/Videos/search', function(req, res) {
        callController('Videos.js').search(app, req, res);
	log(app, req, res);
    });

    app.post('/Videos/login', function (req, res){
	callController('userauth.js').login(app, req, res);
	//console.log(req.sessionID);
	log(app, req, res);
    });

    app.use(function(req, res, next) {
	if( req.path === '/Videos/perso' ) {
		if( callController('userauth.js').is_granted(app, req, req.path, res) ) {
			//callController('Videos.perso.js')(app, req, res);
			videoPerso.process(app, req, res);
		}
		else
			res.redirect('/Videos');
        	log(app, req, res);
	}
	else
		next();
    });

	app.use('/Videos/perso/files/*',function (req, res, next){
	var realpath = url.parse(req.originalUrl).pathname;
	var user = null;
	var userAuth = null;
	req.session.save(function(err){
		var store = app.get('store');
		/*store.get( req.sessionID, function( error, data ) {
			//var masession = sessions[];
			//var tmpuserAuth = data['userAuth'];
			if( error == null )
				console.log('1111 !!!!!!!!!!!! ' + req.sessionID + ':' + data.userAuth.user.username); //+ JSON.stringify(data));
		});*/
		if( typeof req.param('id') !== 'undefined' ) {
		store.get( req.param('id'), function( error, data ) {
			//var masession = sessions[];
			//var tmpuserAuth = data['userAuth'];
			if( error == null ) {

				if( typeof data !== 'undefined' && data != null && typeof data.userAuth !== 'undefined' && data.userAuth != null ) {
					user = data.userAuth.user;
					userAuth = new UserAuth(user);
					if( !userAuth.is_granted_route(realpath) ) {
						res.redirect('/Videos');
					} else
						next();
				} else
					res.redirect('/Videos');
			} else
				res.redirect('/Videos');
		});
		} else {
			
			if( typeof req.session  !== 'undefined' && req.session.userAuth != null && typeof req.session.userAuth !== 'undefined') { user = req.session.userAuth.user ; userAuth = new UserAuth(user);}
			if( userAuth == null || !userAuth.is_granted_route(realpath) ) {
				res.redirect('/Videos');
			}
			else
				next();

		}
	});

	
	log(app, req, res);
        });


};


function callController(name) {
        var realpath =__dirname + '/../app/controllers';
        return require(realpath + '/' + name); //(app, req, res);
};

function callDataModel(name) {
        var realpath =__dirname + '/../app/models';
        return require(realpath + '/' + name); //(app, req, res);
};

function log(app, req, res )
{
        console.log(require('process').pid + "/ %s %s %s %s",req.method,req.originalUrl,res.statusCode,req.ip );
};

