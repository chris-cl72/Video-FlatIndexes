Video-FlatIndexes
=================
Le but de ce projet est de mettre en oeuvre un modèle MVC basé sur Node.js, au travers
une application d'indexation de fichiers Videos de type films/series.

![Aperçu de la page d'accueil](screenshots/Capture_FVI_1.jpg?raw=true "Page d'accueil")
![Aperçu de la page des nouveautés](screenshots/Capture_FVI_2.jpg?raw=true "Page des nouveautés")
![Aperçu de la page des films triés par genre](screenshots/Capture_FVI_3.jpg?raw=true "Page des films triés par genre")
![Aperçu de la page des séries télé triées par titre](screenshots/Capture_FVI_4.jpg?raw=true "Page des séries télé triées par titre")
![Aperçu de la page de Téléchargement pour le renomage des fichiers](screenshots/Capture_FVI_5.jpg?raw=true "Page de Téléchargement pour le renomage des fichiers")
![Aperçu de la page de Téléchargement pour les propositions d'indexation](screenshots/Capture_FVI_6.jpg?raw=true "Page de Téléchargement pour les propositions d'indexation")