var fs = require('fs');
var path = require('path');
var Mkvmerge = require('mkvmerge');
var async = require('async');


function callDataModel(name) {
        var mypath =__dirname + '/../models';
        return require(mypath + '/' + name);
};

function clone(obj) {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
};

var VideoPerso = function() {
	var LocalVideos = callDataModel('localVideos.js');
	var LocalDownloads = callDataModel('localDownloads.js');
	var OnlineVideos = callDataModel('onlineVideos.js');
	var UserAuth = callDataModel('userauth.js');	

	var user = null;

	//console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! new queue');
	/*var queue = async.queue(function(objectQueue, callback) {
		//console.log('EEEEEEEEEEEEEEEEEntry');
		objectQueue.req.session.reload(function(err) {									
			var clonetaskObjects = new Object();
			if( typeof objectQueue.req.session.taskObjects !== 'undefined' ) {
				clonetaskObjects = clone(objectQueue.req.session.taskObjects);
			}
			var matask = clonetaskObjects[objectQueue.key]
			if( matask == null || typeof matask === 'undefined' ) 
				matask = objectQueue.mkvmerge;
			matask.progress = objectQueue.progress;
			matask.errors = objectQueue.errors;
			matask.running = objectQueue.running;
			matask.exitcode = objectQueue.exitcode;
			matask.key = objectQueue.key;

			clonetaskObjects[objectQueue.key] = matask;
			objectQueue.req.session.taskObjects = clone(clonetaskObjects);										
			objectQueue.req.session.save( function(err) {
				//console.log('sauvegarder : ' + objectQueue.key + ' -> ' + JSON.stringify(objectQueue.req.session.taskObjects)); 
				callback();
			});
		});
	}, 1); 

	queue.drain = function() {
		//checkDone();
		//console.log('SSSSSSSSSSSSSSSSortie');
	};*/


	this.process = function(app, req, res) {
		if( req.session.userAuth != null && typeof req.session.userAuth !== 'undefined') { user = req.session.userAuth.user }
		var userAuth = new UserAuth(user);
		if( req.method === 'GET' ) {
			if( req.param('type') === 'listvideos'  &&  req.param('typevideo') === 'all' ) {
				if( req.param('order') === 'news' ) {
					//var localVideos = this.getlocalVideosSession(app, req);
					this.getlocalVideosSession(req.sessionID, function(err, localVideos) { 

						var list = localVideos.getLastFilms(10);
								
						var list1 = localVideos.getLastSeries(5);
						for (var i = 0; i < list1.length; i++) {
							list[list.length] = list1[i];
						}
						if( req.param('actiontype') !== ''  &&  req.param('actionvalue') !== '' )
							res.render('Videos.perso.twig', { userAuth : userAuth, lastFilms : list, id : req.sessionID, actiontype : req.param('actiontype'), actionvalue : req.param('actionvalue') });
						else
							res.render('Videos.perso.twig', { userAuth : userAuth, lastFilms : list, id : req.sessionID });
					});
				}
			}
			else if( req.param('type') === 'listvideos'  &&  req.param('typevideo') === 'movie' ) {
				if( req.param('order') === 'genre' ) {
					//var localVideos = this.getlocalVideosSession(app, req);
					this.getlocalVideosSession(req.sessionID, function(err, localVideos) { 
						var list = localVideos.getLastFilmsbyGenre(req.param('value'));
						res.json({ userAuth : userAuth, list : list, id : req.sessionID });
					});
				}
				else if( req.param('order') === 'year' ) {
					//var localVideos = this.getlocalVideosSession(app, req)
					this.getlocalVideosSession(req.sessionID, function(err, localVideos) { 
				     	var list = localVideos.getLastFilmsbyYear(req.param('value'));
				    	res.json({ userAuth : userAuth, list : list, id : req.sessionID });
					});
			   	}
				else if( req.param('order') === 'country' ) {
					//var localVideos = this.getlocalVideosSession(app, req);
					this.getlocalVideosSession(req.sessionID, function(err, localVideos) { 
				    	var list = localVideos.getLastFilmsbyCountry(req.param('value'));
				    	res.json({ userAuth : userAuth, list : list, id : req.sessionID });
					});
				}
				else if( req.param('order') === 'group' ) {
					//var localVideos = this.getlocalVideosSession(app, req);
					this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
				    	var list = localVideos.getLastFilmsbyGroup(req.param('value'));
				    	res.json({ userAuth : userAuth, list : list, id : req.sessionID });
					});
				}
				else
					res.redirect('/Videos');
			}
			else if( req.param('type') === 'listgenres' &&  req.param('typevideo') === 'movie' ) {
				/*var localVideos = this.getlocalVideosSession(app, req);
				var list = localVideos.getMovieListGenres();		
				res.json(list);	*/	
				//var localVideos = this.getlocalVideosSession(app, req);
				this.getlocalVideosSession(req.sessionID, function(err, localVideos) { 
					var list = localVideos.getMovieListGenres();
					res.json(list);	
				});
			}
			else if( req.param('type') === 'listyears' &&  req.param('typevideo') === 'movie' ) {
				//var localVideos = this.getlocalVideosSession(app, req);
				this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
				    var list = localVideos.getMovieListYears();
				    res.json(list);
				});
			}
			else if( req.param('type') === 'listcountrys' &&  req.param('typevideo') === 'movie' ) {
				//var localVideos = this.getlocalVideosSession(app, req);
				this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
				    var list = localVideos.getMovieListCountrys();
				    res.json(list);
				});
			}
			else if( req.param('type') === 'listgroups' &&  req.param('typevideo') === 'movie' ) {
				//var localVideos = this.getlocalVideosSession(app, req);
				this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
				    var list = localVideos.getMovieListGroups();
				    res.json(list);
				});
			}
			else if( req.param('type') === 'listvideos'  &&  req.param('typevideo') === 'tvserie' ) {
				if( req.param('order') === 'title' ) {
					//var localVideos = this.getlocalVideosSession(app, req);
					this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
						var list = localVideos.getLastSeriesbyTitle(req.param('value'));
						res.json({ userAuth : userAuth, list : list, id : req.sessionID });
					});
				}
				else if( req.param('order') === 'genre' ) {
					//var localVideos = this.getlocalVideosSession(app, req);
					this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
						var list = localVideos.getLastSeriesbyGenre(req.param('value'));
						res.json({ userAuth : userAuth, list : list, id : req.sessionID });
					});
				}
				else if( req.param('order') === 'year' ) {
					//var localVideos = this.getlocalVideosSession(app, req);
					this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
				    	var list = localVideos.getLastSeriesbyYear(req.param('value'));
				    	res.json({ userAuth : userAuth, list : list, id : req.sessionID });
					});
				}
				else if( req.param('order') === 'country' ) {
					//var localVideos = this.getlocalVideosSession(app, req);
					this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
				    	var list = localVideos.getLastSeriesbyCountry(req.param('value'));
				    	res.json({ userAuth : userAuth, list : list, id : req.sessionID });
					});
				}
				else if( req.param('order') === 'group' ) {
					//var localVideos = this.getlocalVideosSession(app, req);
					this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
				    	var list = localVideos.getLastSeriesbyGroup(req.param('value'));
				    	res.json({ userAuth : userAuth, list : list, id : req.sessionID });
					});
				}
				else
					res.redirect('/Videos');
			}
			else if( req.param('type') === 'listtitles' &&  req.param('typevideo') === 'tvserie' ) {
				//var localVideos = this.getlocalVideosSession(app, req);
				this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
					var list = localVideos.getSerieListTitles();		
					res.json(list);		
				});
			}
			else if( req.param('type') === 'listgenres' &&  req.param('typevideo') === 'tvserie' ) {
				//var localVideos = this.getlocalVideosSession(app, req);
				this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
					var list = localVideos.getSerieListGenres();		
					res.json(list);	
				});	
			}
			else if( req.param('type') === 'listyears' &&  req.param('typevideo') === 'tvserie' ) {
				//var localVideos = this.getlocalVideosSession(app, req);
				this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
				    var list = localVideos.getSerieListYears();
				    res.json(list);
				});
			}
			else if( req.param('type') === 'listcountrys' &&  req.param('typevideo') === 'tvserie' ) {
				//var localVideos = this.getlocalVideosSession(app, req);
				this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
				    var list = localVideos.getSerieListCountrys();
				    res.json(list);
				});
			}
			else if( req.param('type') === 'listgroups' &&  req.param('typevideo') === 'tvserie' ) {
				//var localVideos = this.getlocalVideosSession(app, req);
				this.getlocalVideosSession(req.sessionID, function(err, localVideos) {
				    var list = localVideos.getSerieListGroups();
				    res.json(list);
				});
			}		
			else if( req.param('type') === 'listdownloads'  && userAuth.is_granted_role('ROLE_ADMIN') ) {
				//deleteSessionData(app,req,'progressobjects');
				//app.set(req.session.progressobjects, null);

				var localDownloads = new LocalDownloads();
				localDownloads.readall(function() { res.json(localDownloads); });			
			}

			else if( req.param('type') === 'getprogress'  && userAuth.is_granted_role('ROLE_ADMIN') ) {
				var key = req.param('key');
				this.getProgress( key, function (value) { 
					var mkvmerge = value;
					if( typeof mkvmerge !== 'undefined' &&  mkvmerge != null) {
						res.json({ error : null, key:key, running : mkvmerge.running, exitcode : mkvmerge.exitcode, progress : mkvmerge.progress });
					}
					else {
						res.json({ error : null, key:key, running : true, exitcode : -1, progress : 0 });
					}
				});			
			}
			else
				res.redirect('/Videos');
		}
		else if (req.method === 'POST') {
			var jsonParams = req.body;
			if( jsonParams['type'] === 'rename' &&  userAuth.is_granted_role('ROLE_ADMIN') ) {
				for(var key in jsonParams){
					if( key !== 'type' && key !== 'typevideo' && key !== 'seasonnumber' ) {
						var localDownloads = new LocalDownloads();
						localDownloads.rename(decodeURIComponent(key),decodeURIComponent(jsonParams[key]), function(oldfile, newfile) {
							res.json({filename:oldfile,data:newfile});
						});					
					}
				}			
		    }
			else if( jsonParams['type'] === 'search' &&  userAuth.is_granted_role('ROLE_ADMIN') ) {
				var searchkeywords = '';
				var filename = '';
				var typevideo = jsonParams['typevideo'];
				var seasonnumber = jsonParams['seasonnumber'];
				for(var key in jsonParams){
					if( key !== 'type' && key !== 'typevideo' && key !== 'seasonnumber' ) {
						filename = key;
						searchkeywords = jsonParams[filename];					
					}
					}
				if( typevideo === 'tvserie' ) {
					var onlineVideos = new OnlineVideos();
					onlineVideos.listseries( searchkeywords,seasonnumber, function(saisons) {
						res.json({filename:filename,typevideo:typevideo,data:saisons});
					});
				}
				if( typevideo === 'movie' ) {
					var onlineVideos = new OnlineVideos();
					onlineVideos.listmovies( searchkeywords,function(movies) {
						res.json({filename:filename,typevideo:typevideo,data:movies});
					});
				}
		     	}
			else if( jsonParams['type'] === 'indexation' &&  userAuth.is_granted_role('ROLE_ADMIN') ) {
				var filename = '';
				var id = 0;
				var typevideo = jsonParams['typevideo'];
				for(var key in jsonParams){
					if( key !== 'type' && key !== 'typevideo' ) {
						filename = key;
						id = jsonParams[filename];
					}
				}	
				if( typevideo === 'tvserie' ) {
					var onlineVideos = new OnlineVideos();
					onlineVideos.getSaison(id, function(masaison) {
						var localDownloads = new LocalDownloads();
						localDownloads.importSerie(decodeURIComponent(filename), masaison, function(error) { 
							if( error )
								console.error(error);
							res.json({filename:filename,masaison:masaison});
						});	
					}); 
				}
				else if( typevideo === 'movie' ) {
					var onlineVideos = new OnlineVideos();
					onlineVideos.getMovie(id, function(monfilm) {
						var localDownloads = new LocalDownloads();
						localDownloads.importFilm(decodeURIComponent(filename), monfilm, function(error) { 
							if( error )
								console.error(error);
							res.json({filename:filename,monfilm:monfilm});
						});	
					}); 
				}   			
			}
			else if( jsonParams['type'] === 'delete' &&  userAuth.is_granted_role('ROLE_ADMIN') ) {
				//deleteSessionData(app,req,'localVideos');
				//app.set( req.session.localVideos, null);	
				this.setlocalVideosSession( req.sessionID, null, function(err) { 		
					if( jsonParams['typevideo'] === 'movie' ) {
						var localVideos = new LocalVideos(null,null);
						localVideos.deleteMovieFile(decodeURIComponent(jsonParams['filename']), function(err) {
							res.json({error:err});
						});
					}
					else if( jsonParams['typevideo'] === 'tvserie' ) {
						var localVideos = new LocalVideos(null,null);
						localVideos.deleteSaisonFile(decodeURIComponent(jsonParams['filename']), function(err) {
							res.json({error:err});
						});
					}
				});
			}
			else if( jsonParams['type'] === 'reindex' &&  userAuth.is_granted_role('ROLE_ADMIN') ) {
				//deleteSessionData(app,req,'localVideos');
				//app.set( req.session.localVideos, null);	
				this.setlocalVideosSession( req.sessionID, null, function(err) { 		
					if( jsonParams['typevideo'] === 'movie' ) {
						var localVideos = new LocalVideos(null,null);
						localVideos.move2downloadMovieFile(decodeURIComponent(jsonParams['filename']), function(err) {
							res.json({error:err});
						});
					}
					/*else if( jsonParams['typevideo'] === 'tvserie' ) {
						var localVideos = new LocalVideos(null,null);
						localVideos.deleteSaisonFile(decodeURIComponent(jsonParams['filename']), function(err) {
							res.json({error:err});
						});
					}*/
				});
			}
			else if( jsonParams['type'] === 'avi2mkv' &&  userAuth.is_granted_role('ROLE_ADMIN') ) {
				//deleteSessionData(app,req,'localVideos');
				var p = this;
				this.setlocalVideosSession( req.sessionID, null, function(err) { 
					var key = jsonParams['key'];
					var desc = jsonParams['desc'];
					var localVideos = new LocalVideos(null,null);
					localVideos.avi2mkv(
						path.join( decodeURIComponent(req.param('path')), decodeURIComponent(req.param('filename')) ),
						function(errors, mkvmerge, running, exitcode, progress) {							
							mkvmerge.key = key;
							mkvmerge.desc = desc; 
							if( errors == null ) {
								p.setProgress( key, null, true, -1, 0, function(err) { 
									res.json({ error : err, key : key, running : true, exitcode : -1, progress : 0,  desc : desc });		
								});
							} else {
								res.json({ error : errors, key : key, running : false, exitcode : -1, progress : 0, desc : desc });
							}
						},
						function(errors, running, exitcode, progress) {
							//queue.push({ mkvmerge: mkvmerge, key : key, req : req, errors : errors, running : running, exitcode : exitcode, progress : progress });
							p.setProgress( key, errors, running, exitcode, progress, function(err) { 
						});
								
					});
				});		
			}			
		}
	};

	this.getlocalVideosSession = function( sessionid, callback) {
		var collection = require('strong-store-cluster').collection('localvideos');
		collection.acquire(sessionid, function(err, keylock, value) {
			var localVideos = null;
			if( typeof value === 'undefined' || value == null )  {
				localVideos = new LocalVideos(null, null);
				localVideos.list("");
			} else {
				localVideos = new LocalVideos(value.films, value.saisons);
			}
			keylock.release();
			collection.set(sessionid, localVideos, function(err) {			
				callback(err, localVideos);
			});
	  	});
	};

	this.setlocalVideosSession = function( sessionid, value, callback) {
		var collection = require('strong-store-cluster').collection('localvideos');
		collection.set(sessionid, value, function(err) {			
			callback(err);
		});
	};

	this.setProgress = function( key, errors, running, exitcode, progress, callback) {
		var collection = require('strong-store-cluster').collection('progress');
		collection.acquire(key, function(err, keylock, value) {
				var matask = {};
				if( typeof value !== 'undefined' && value != null && value != 'undefined'  )  {
					matask = value;
				}
				matask.progress = progress;
				matask.errors = errors;
				matask.running = running;
				matask.exitcode = exitcode;
				matask.key = key;
				keylock.release();
				collection.set(key, matask, function(err) {		
					callback(err);
				});
	  	});
	};

	this.getProgress = function( key, callback ) {
		var collection = require('strong-store-cluster').collection('progress');
		collection.acquire(key, function(err, keylock, value) {
			callback(value);
			keylock.release();
	  	});
	};
	/*this.getlocalVideosSession = function (app, req, callback) {
			var localVideos = null;
			req.session.reload( function(err) {
			if( typeof req.session.localVideos === 'undefined' || req.session.localVideos == null ) {
				localVideos = new LocalVideos(null, null);
				localVideos.list("");
				//req.session.localVideos = localVideos;
				req.session.save(function(err) { });
				//req.session.localVideos = {};
				//req.session.localVideos.films = localVideos.films;
			}
			else { 
				//console.log('oooooooooooooooold 1111111111');
				localVideos = new LocalVideos(req.session.localVideos.films, req.session.localVideos.saisons);
				//console.log('oooooooooooooooold 2222222222');
			}
				callback(localVideos);
			});
			//return localVideos;
	};*/
}

module.exports = VideoPerso;



